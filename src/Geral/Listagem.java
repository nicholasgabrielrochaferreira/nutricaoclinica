package Geral;

import Geral.Conexao;
import javax.swing.*;
import java.sql.*;
import java.util.logging.*;

public class Listagem {

    public void listar(JList lista, String tabela) {
        try {
            lista.removeAll();
            DefaultListModel dlm = new DefaultListModel();
            Connection c = Conexao.obterConexao();
            String SQL = "select * from scnutricaoclinica." + tabela;
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(SQL);
            while (rs.next()) {
                if (tabela.equals("alimento")) {
                    dlm.addElement(rs.getString("nome"));
                } else if(tabela.equals("cliente")){
                    dlm.addElement(rs.getString("nomeCliente"));
                } else if(tabela.equals("consulta")){
                    dlm.addElement(rs.getString("data_hora"));
                } else if(tabela.equals("dieta")){
                    dlm.addElement(rs.getString("nomeDieta"));
                }
            }
            lista.setModel(dlm);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Listagem.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
