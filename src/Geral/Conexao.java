package Geral;

import java.sql.*;

public class Conexao {

    public static Connection obterConexao() {
        try {
            Class.forName("org.postgresql.Driver");
            String banco = "jdbc:postgresql://127.0.0.1/NutricaoClinicaDb";
            String usuario = "Administrador";
            String senha = "admin";
            return DriverManager.getConnection(banco, usuario, senha);
        } catch (ClassNotFoundException | SQLException ex) {
            return null;
        }
    }
}
