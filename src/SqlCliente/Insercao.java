package SqlCliente;

import Geral.Conexao;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Insercao {

    public void Inserir(String nomeCliente, String telefone, String rg, String e_mail, String tipoDeAlimentacao) {
        try {
            Connection c = Conexao.obterConexao();
            String SQL = "insert into scnutricaoclinica.cliente(nomeCliente, telefone, rg, e_mail, tipoDeAlimentacao) values(?, ?, ?, ?, ?)";
            PreparedStatement ps = c.prepareStatement(SQL);

            ps.setString(1, nomeCliente);
            ps.setString(2, telefone);
            ps.setString(3, rg);
            ps.setString(4, e_mail);
            ps.setString(5, tipoDeAlimentacao);
            
            ps.executeUpdate();
            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
