package SqlAlimento;

import Geral.Conexao;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Insercao {

    public void Inserir(String nome, String valorEnergetico, String carboidratos, String proteinas, String gorduras) {
        try {
            Connection c = Conexao.obterConexao();
            String SQL = "insert into scnutricaoclinica.alimento(nome, valorEnergetico, carboidratos, proteinas, gorduras) values(?, ?, ?, ?, ?)";
            PreparedStatement ps = c.prepareStatement(SQL);

            ps.setString(1, nome);
            ps.setString(2, valorEnergetico);
            ps.setString(3, carboidratos);
            ps.setString(4, proteinas);
            ps.setString(5, gorduras);
            
            ps.executeUpdate();
            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
