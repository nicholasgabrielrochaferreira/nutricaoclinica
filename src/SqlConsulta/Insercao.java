package SqlConsulta;

import Geral.Conexao;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Insercao {

    public void Inserir(String data_hora, String nomeCliente, String relatorio) {
        try {
            Connection c = Conexao.obterConexao();
            String SQL = "insert into scnutricaoclinica.consulta(data_hora, nomeCliente, relatorio) values(?, ?, ?)";
            PreparedStatement ps = c.prepareStatement(SQL);

            ps.setString(1, data_hora);
            ps.setString(2, nomeCliente);
            ps.setString(3, relatorio);
            
            ps.executeUpdate();
            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
